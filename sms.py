import os
import json
import openai
import random
import requests
from PIL import Image, ImageDraw, ImageFont


def get_response(model: str, prompt: str, max_tokens: int = 100):
    if model == "gpt-3.5-turbo":
        response = openai.ChatCompletion.create(
            model=model,
            messages=[{"role": "user", "content": prompt}],
            temperature=0.6,
            max_tokens=max_tokens,
        )
        return response['choices'][0]['message']['content']
    else:
        response = openai.Completion.create(
                model=model,
                prompt=prompt,
                temperature=0.6,
                max_tokens=max_tokens
        )
        return response['choices'][0]['text']


def read_json_file(file_path):
    with open(file_path, 'r') as file:
        data = json.load(file)
    return data


def save_image_from_url(url, save_path):
    try:
        # Send an HTTP GET request to the URL to fetch the image
        response = requests.get(url, stream=True)
        response.raise_for_status()  # Raise an exception if the request was unsuccessful

        # Check if the save path exists; if not, create the necessary directories
        save_directory = os.path.dirname(save_path)
        os.makedirs(save_directory, exist_ok=True)

        # Open the file in binary write mode and save the image content
        with open(save_path, 'wb') as file:
            for chunk in response.iter_content(chunk_size=8192):
                file.write(chunk)
        print(f"Image saved successfully as {save_path}")
    except Exception as e:
        print(f"Error saving the image: {e}")


def save_image_with_caption(image_path, output_image_path, caption_text, text_split_param: int = 6):
    # Split caption to rows
    caption_text_split = caption_text.split(' ')
    caption_text_split = [caption_text_split[i:i + text_split_param] for i in range(0, len(caption_text_split),
                                                                                    text_split_param)]
    caption_text_split = [' '.join(cts) for cts in caption_text_split]
    caption_text_split = '\n'.join(caption_text_split)

    # Load the input image
    image = Image.open(image_path)

    # Get the dimensions of the input image
    image_width, image_height = image.size

    # Set the font size and font file path
    font = ImageFont.load_default()

    # Calculate the size of the caption text
    text_width, text_height = ImageDraw.Draw(Image.new('RGB', (1, 1))).textsize(caption_text_split, font=font)
    text_width /= text_split_param

    # Calculate the size of the new image that will accommodate the original image and the caption
    new_image_width = max(image_width, text_width)
    new_image_height = image_height + text_height + 20  # Add some padding between the image and the caption

    # Create a new blank image with the calculated dimensions
    new_image = Image.new("RGB", (new_image_width, new_image_height), color=(255, 255, 255))

    # Paste the original image on the new blank image
    new_image.paste(image, ((new_image_width - image_width) // 2, 0))

    # Create a drawing context on the new image
    draw = ImageDraw.Draw(new_image)

    # Calculate the position to place the caption at the bottom center of the image
    caption_x = 10  # (new_image_width - text_width) // 2
    caption_y = image_height + 10

    # Set the caption text color (black in this example)
    text_color = (0, 0, 0)

    # Draw the caption on the new image
    draw.text((caption_x, caption_y), caption_text_split, font=font, fill=text_color)

    # Save the modified image with caption as a new PNG file
    new_image.save(output_image_path)

    print(f"Image with caption saved successfully to {output_image_path}")


def main(num_topics_selected: int = 2,
         image_size: int = 512,
         openai_api_key: str = "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",
         output_dir: str = './generated_images'):
    # Set openai key
    openai.api_key = openai_api_key

    # Access the list of words
    topics = read_json_file('./topics.json')
    topics = topics['topics']

    # Select topics
    selected_topics = random.sample(topics, num_topics_selected)

    # Select model
    model = "text-davinci-003"

    # Get image generation prompt
    prompt_1 = f"You will now act as a prompt generator. I will give you a list of words and you will create a text " \
               f"prompt that could be used for a generative text-to-image model. " \
               f" The list of the words is the following: {', '.join(selected_topics)}. " \
               f"Answer with a text prompt that includes the words from the list. " \
               f"The text prompt should be meaningful, specific and describe a sensible image. " \
               f"The text prompt should be used as a conditioning " \
               f"The text prompt should be less than 20 words separated by commas." \
               f"In the text prompt describe some specific photo characteristics from this list (not all of them):" \
               f"lightning, aesthetics, composition, quality, perspective, background, color balance, focal point, " \
               f"camera device brand and model, depth of field."
    image_generation_prompt = get_response(model=model, prompt=prompt_1).replace('\n', '').replace('"', '')[:-1]
    image_generation_prompt += ', best quality, fullhd, 4k, 8k, masterpiece, stunning'
    print(f"response 1: {image_generation_prompt}")

    # Get hashtags
    prompt_2 = f"Generate hashtags for an image that was generated with the following text prompt:" \
               f" {image_generation_prompt}." \
               f"Answer with a list of hashtags separated by commas."
    hashtags = get_response(model=model, prompt=prompt_2).replace('\n', "")
    print(f"response 2: {hashtags}")

    # Get image
    response = openai.Image.create(
        prompt=image_generation_prompt,
        n=1,
        size=f"{image_size}x{image_size}"
    )
    image_url = response['data'][0]['url']
    print(f"Generated image is available at {image_url}")

    # Save image
    num_sub_dirs = len(os.listdir(output_dir)) - 1  # "-1" is used to exclude .DS_Store dir
    output_name = f"{num_sub_dirs + 1:03d}"
    output_sub_dir = os.path.join(output_dir, output_name)
    os.makedirs(output_sub_dir)
    output_image_path = os.path.join(output_sub_dir, f"img_{output_name}.png")
    save_image_from_url(url=image_url, save_path=output_image_path)

    # Save image with caption
    output_image_with_caption_path = os.path.join(output_sub_dir, f"img_with_caption_{output_name}.png")
    caption_text = f"Caption: {image_generation_prompt}\n\nHashtags: {hashtags}"
    save_image_with_caption(image_path=output_image_path, output_image_path=output_image_with_caption_path,
                            caption_text=caption_text)

    # Save caption and hashtags
    output_file_path = os.path.join(output_sub_dir, f"caption_{output_name}.txt")
    with open(output_file_path, 'w') as file:
        file.write(caption_text)
    print(f"Caption and hashtags saved successfully as {output_file_path}")


if __name__ == "__main__":
    main()
