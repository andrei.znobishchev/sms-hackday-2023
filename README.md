# Social Media Scheduler

### HackDay2023. Picsart
### Team: SMS 

Envision a world where a powerful Text2Image model breathes life into your favorite topics, be it vintage cars, elite watches, or rare airplanes. If you yearn to see your passions visualized in all their glory, a mere "beautiful vintage cars" won't suffice. Enter ChatGPT - your key to crafting the perfect text prompt that transcends the ordinary. Moreover, it has the ability to blend various topics of your interest seamlessly, forming a meaningful scene.

Thus, all you need is to specify your favourite topics and ChatGPT will do the rest of the work:
- Generate a meaningful text prompt combining several of your favourite topics with detailed description of lightning, aesthetics, composition, quality, perspective, a camera device and so on.
- Generate tailored hashtags based on your text prompt, catapulting your content to new heights on social media platforms.
- Send the request to the Text2Image model (DALLE in the case of our demo for simplicity, i.e. to install only openai library).

Prepare to mesmerize your audience with an infinite array of visually arresting wonders. Text2Image opens the door to a world where creativity knows no bounds. Embrace the magic of ChatGPT and witness your dreams manifest into artistry that is simply unparalleled.

Are you ready to step into this realm of boundless possibilities? Simply contemplate your favorite topics once, and behold an infinite array of diverse content, primed and ready for publication on social media platforms!



The following image was generated using the DALLE model (https://platform.openai.com/docs/guides/images/image-generation-beta)
from the text prompt that was 
automatically generated with ChatGPT along with hashtags for this image:
<p align="center">
  <img width="512" height="753" src=./generated_images/001/img_with_caption_001.png>
</p>


## Installation
```
pip install openai
pip install Pillow==9.5.0
```

## Demo
`python sms.py`
